package pl.cm.calc.io;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class ExtOutputWriter implements OutputWriter {

    @Override
    public void write(final String message) {
        System.out.println("*" + message);
    }
}
