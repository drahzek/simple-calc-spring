package pl.cm.calc.calculation;

import org.springframework.stereotype.Service;
import pl.cm.calc.name.NameProvider;
@Service
public class Sub extends NameProvider implements Calculation {

  public Sub() {
    super("sub");
  }

  public long calculate(long first, long second) {
    return first - second;
  }

}
