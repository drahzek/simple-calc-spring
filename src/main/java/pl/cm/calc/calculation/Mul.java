package pl.cm.calc.calculation;

import org.springframework.stereotype.Service;
import pl.cm.calc.name.NameProvider;
@Service
public class Mul extends NameProvider implements Calculation {

    public Mul() {
        super("mul");
    }

    @Override
    public long calculate(long first, long second) {
        return first * second;
    }

}