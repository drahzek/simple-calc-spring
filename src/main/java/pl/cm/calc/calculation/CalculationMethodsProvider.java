package pl.cm.calc.calculation;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CalculationMethodsProvider {
    private Add add;
    private Sub sub;
    private Mul mul;
    private Div div;

    @Autowired
    private List<Calculation> calculations;

    public List<Calculation> getAvailableCalculationMethods() {
    return calculations;
  }
    public CalculationMethodsProvider(Add add, Sub sub, Mul mul, Div div) {
        this.add = add;
        this.sub = sub;
        this.mul = mul;
        this.div = div;
    }
}
