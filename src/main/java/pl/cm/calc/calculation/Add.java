package pl.cm.calc.calculation;

import org.springframework.stereotype.Service;
import pl.cm.calc.name.NameProvider;

@Service
public class Add extends NameProvider implements Calculation {

  public Add() {
    super("add");
  }

  @Override
  public long calculate(long first, long second) {
    return first + second;
  }

}
