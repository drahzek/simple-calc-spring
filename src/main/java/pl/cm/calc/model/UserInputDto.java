package pl.cm.calc.model;

public class UserInputDto {
  private final String operation;
  private final long leftOperand;
  private final long rightOperand;

  public UserInputDto(final String operation, final long leftOperand, final long rightOperand) {
    this.operation = operation;
    this.leftOperand = leftOperand;
    this.rightOperand = rightOperand;
  }

  public String getOperation() {
    return operation;
  }

  public long getLeftOperand() {
    return leftOperand;
  }

  public long getRightOperand() {
    return rightOperand;
  }
}
