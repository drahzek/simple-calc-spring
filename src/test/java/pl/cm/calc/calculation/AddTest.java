package pl.cm.calc.calculation;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class AddTest {

  private final Add instance = new Add();
  private final long leftOperand;
  private final long rightOperand;

  @Parameterized.Parameters
  public static Collection<Object[]> data() {
    List<Object[]> parameters = new ArrayList<>();
    parameters.add(new Integer[]{ 1, 2 });
    parameters.add(new Integer[]{ 10, 10 });
    return parameters;
  }

  public AddTest(long leftOperand, long rightOperand) {
    this.leftOperand = leftOperand;
    this.rightOperand = rightOperand;
  }

  @Test
  public void should_add_max() {
    final long result = instance.calculate(leftOperand, rightOperand);

    assertThat(result).isEqualTo(leftOperand + rightOperand);
  }


  @Test
  public void should_return_proper_name() {
    final String name = instance.getName();

    assertThat(name).isEqualTo("add");
  }
}